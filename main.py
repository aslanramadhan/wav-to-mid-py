# Import All GUI Library
from tkinter import *

# Import File Dialog GUI
from tkinter import filedialog

import numpy as np
import wave
import sys
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


# File Explorer Method
def openExplorer() :
    global filename
    filename = filedialog.askopenfilename(initialdir="/",
                                          title="Select a File",
                                          filetypes=(("WAV Files","*.wav*"),
                                                     ("All Files","*.*")))
    lblFileName.configure(text="File Opened : "+filename)

def wavExtract() :
    spf = wave.open(filename)
    signal = spf.readframes(-1)
    signal = np.fromstring(signal,"Int16")
    fs = spf.getframerate()
    if spf.getnchannels() == 2 :
        print("This file is audio with mono type")
    Time = np.linspace(0,len(signal)/fs, num=len(signal))
    plt.figure(1)
    plt.title("Signal Wave")
    plt.plot(Time,signal)
    lines = FigureCanvasTkAgg(plt, window)
    lines.get_tk_widget().pack(side=LEFT,fill=BOTH)


# Create Main Window
global window
window = Tk()

# Set Window Title
window.title('WAV to MIDI Converter')

# Window Size
window.geometry("750x500")

# Set Window Background Color
window.config(background="white")

# Label File Name
lblFileName = Label(window, text="Browse File WAV", bg="white")
btnExplore = Button(window, text="Browse File", command=openExplorer)
btnConvert = Button(window, text="Convert to MIDI", command=wavExtract)

# Grid Layout
lblFileName.grid(column=1, row=1)
btnExplore.grid(column=1, row=2)
btnConvert.grid(column=2, row=2)

window.mainloop()